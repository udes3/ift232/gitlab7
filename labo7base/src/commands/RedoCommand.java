package commands;

public class RedoCommand extends Command{

	private CommandLog log;

	public RedoCommand(CommandLog l) {
		log =l;
	}

	@Override
	public void execute() {
		log.redo();

	}
}
