package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class CopyCommand extends EditDocumentCommand{

	public CopyCommand(EditableDocument d, EditorTextArea a, CommandLog l) {
		super(d, a, l);
	}

	@Override
	public void executeSpecific() {
		model.copy(textBox.getSelectionStart(),textBox.getSelectionEnd());	
		
	}

	@Override
	public void saveState() {
		oldData = model.getText();
		text = textBox.getSelectedText();
		cursor = textBox.getCaretPosition();
	}

	@Override
	public void redo() {
		model.setText(oldData);
	}

	

}
