package commands;

import java.util.Stack;

public class CommandLog {

	private Stack<EditDocumentCommand> undoCommands = new Stack<EditDocumentCommand>();
	private Stack<EditDocumentCommand> redoCommands = new Stack<EditDocumentCommand>();

	public void addCommand(EditDocumentCommand com) {
		undoCommands.push(com);
	}

	public void undo() {
		System.out.println("Pr� undo : " + undoCommands.toString());
		
		if (undoCommands.size() > 0) {
			EditDocumentCommand x = undoCommands.pop();
			x.undo();
			redoCommands.push(x);
			
			System.out.println(x);

            System.out.println("POST undo : " + undoCommands.toString());
			
		}else
            System.out.println("Pile vide :(");
	}
	
	public void redo() {
		System.out.println("Pr� redo : " + redoCommands.toString());
		
		if (redoCommands.size() > 0) {
			EditDocumentCommand y = redoCommands.pop();
			y.redo();
			undoCommands.push(y);
			
			System.out.println(y);
			
			System.out.println("POST redo : " + redoCommands.toString());
		}
		else
			System.out.println("Pas de Redo disponible");
	}
	
	public boolean emptyUndo() {
		return undoCommands.size() == 0;
	}
	
	public boolean emptyRedo() {
		return redoCommands.size() == 0;
	}
}
