package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public abstract class EditDocumentCommand extends Command implements Cloneable {
	protected EditableDocument model;
	protected EditorTextArea textBox;
	protected CommandLog log;

	protected String oldData = "";
	protected char caractere;
	protected int cursor;
	protected String text;
	
	public abstract void saveState();

	public EditDocumentCommand(EditableDocument d, EditorTextArea a, CommandLog l) {

		model = d;
		textBox = a;
		log = l;
	}

	public void undo() {
		model.setText(oldData);
	}
	
	public abstract void redo();

	@Override
	public void execute() {
		System.out.println("element ajoute");
		saveState();
		executeSpecific();
		log.addCommand(this.clone());
	}

	protected abstract void executeSpecific();

	public EditDocumentCommand clone() {
		try {
			return (EditDocumentCommand) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

}
