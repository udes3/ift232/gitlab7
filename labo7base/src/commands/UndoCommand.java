package commands;

public class UndoCommand extends Command{
	
	private CommandLog log;
	
	public UndoCommand(CommandLog l) {
		log =l;
	}

	@Override
	public void execute() {
		log.undo();
		
	}

}
