package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class MinCommand extends EditDocumentCommand{

	public MinCommand(EditableDocument d, EditorTextArea a, CommandLog l) {
		super(d, a, l);
	}

	@Override
	public void executeSpecific() {
		model.minimize(textBox.getSelectionStart(),textBox.getSelectionEnd());
		
	}

	@Override
	public void saveState() {
		oldData = model.getText();
		text = textBox.getSelectedText();
		cursor = textBox.getCaretPosition();
		
	}

	@Override
	public void redo() {
		StringBuilder stb = new StringBuilder(oldData);
		stb.replace(cursor - text.length(), cursor, text.toLowerCase());
		model.setText(stb.toString());
		
	}

}
