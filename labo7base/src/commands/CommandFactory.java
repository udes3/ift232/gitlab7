package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorCheckBox;
import labo7.ui.EditorTextArea;

public class CommandFactory{
	
	private static CommandFactory INSTANCE = null;
	private CommandLog log = new CommandLog();
	
	public CommandLog getLog() {
		return log;
	}

	public static synchronized CommandFactory getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new CommandFactory();
		}
		return INSTANCE;
	}

	public PasteCommand createPasteCommand(EditableDocument d, EditorTextArea a) {
		return new PasteCommand(d, a, log);
	}
	
	public CopyCommand createCopyCommand(EditableDocument d, EditorTextArea a) {
		return new CopyCommand(d, a, log);
	}
	
	public CutCommand createCutCommand(EditableDocument d, EditorTextArea a) {
		return new CutCommand(d, a, log);
	}
	
	public MajCommand createMajCommand(EditableDocument d, EditorTextArea a) {
		return new MajCommand(d, a, log);
	}
	
	public MinCommand createMinCommand(EditableDocument d, EditorTextArea a) {
		return new MinCommand(d, a, log);
	}
	
	public ToggleInsertCommand createToggleInsertCommand(EditableDocument d, EditorCheckBox b) {
		return new ToggleInsertCommand(d, b);
		
	}
	
	public TwitCommand createTwitCommand(EditableDocument d, EditorTextArea a) {
		return new TwitCommand(d, a, log);
		
	}
	public UndoCommand createUndoCommand() {
		return new UndoCommand(log);
		
	}

	public Command createRedoCommand() {
		return new RedoCommand(log);
	}
	
	public Command createEditTextCommand(EditableDocument d, EditorTextArea a) {
		return new EditTextCommand(d, a, log);
	}
}
