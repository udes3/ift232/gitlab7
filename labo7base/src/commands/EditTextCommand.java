package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class EditTextCommand extends EditDocumentCommand{

	public EditTextCommand(EditableDocument d, EditorTextArea a, CommandLog l) {
		super(d, a, l);
	}

	@Override
	public void saveState() {
		cursor = textBox.getCaretPosition();
		
	}

	@Override
	public void redo() {
		StringBuilder stb = new StringBuilder(oldData);
		stb.insert(cursor, String.valueOf(caractere));
		model.setText(stb.toString());
	}

	@Override
	protected void executeSpecific() {
		StringBuilder stb = new StringBuilder(model.getText());
		saveState();
		caractere = stb.charAt(cursor);
		oldData = stb.deleteCharAt(cursor).toString();
	}

}
