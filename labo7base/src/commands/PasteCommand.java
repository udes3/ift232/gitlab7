package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class PasteCommand extends EditDocumentCommand{

	public PasteCommand(EditableDocument d, EditorTextArea a, CommandLog l) {
		super(d, a, l);
	}

	@Override
	public void executeSpecific() {
		model.paste(textBox.getSelectionStart());
		
	}
	
	@Override
	public void saveState() {
		oldData = model.getText();
		cursor = textBox.getCaretPosition();
		
	}

	@Override
	public void redo() {
		StringBuilder stb = new StringBuilder(oldData);
		stb.insert(cursor, model.getClipBoard());
		model.setText(stb.toString());
		
	}

}
