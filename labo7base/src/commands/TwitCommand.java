package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class TwitCommand extends EditDocumentCommand {

	public TwitCommand(EditableDocument d, EditorTextArea a, CommandLog l) {
		super(d, a, l);
	}

	@Override
	public void executeSpecific() {
		if (model.getText().length() > 140) {
			model.setText(model.getText().substring(0, 140));

		}
	}

	@Override
	public void saveState() {
		oldData = model.getText();
		
	}

	@Override
	public void redo() {
		model.setText(model.getText().substring(0, 140));
		
	}

}
