package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class MajCommand extends EditDocumentCommand{

	public MajCommand(EditableDocument d, EditorTextArea a, CommandLog l) {
		super(d, a, l);
	}

	@Override
	public void executeSpecific() {
		model.capitalize(textBox.getSelectionStart(),textBox.getSelectionEnd());
		
	}

	@Override
	public void saveState() {
		oldData = model.getText();
		text = textBox.getSelectedText();
		cursor = textBox.getCaretPosition();
		
	}

	@Override
	public void redo() {
		StringBuilder stb = new StringBuilder(oldData);
		stb.replace(cursor - text.length(), cursor, text.toUpperCase());
		model.setText(stb.toString());
		
	}

}
