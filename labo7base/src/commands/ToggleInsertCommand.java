package commands;

import labo7.model.EditableDocument;
import labo7.ui.EditorCheckBox;

public class ToggleInsertCommand extends Command{
	
	private EditorCheckBox box;
	private EditableDocument model;

	public ToggleInsertCommand(EditableDocument m, EditorCheckBox b) {
		
		model = m;
		box = b;
	}

	@Override
	public void execute() {
		
		if (box.isSelected()) {
			model.enableInsert();
		} else {
			model.disableInsert();
		}
		
	}

}
