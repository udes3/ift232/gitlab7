package labo7.ui;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import commands.Command;
import commands.CommandFactory;
import commands.CommandLog;
import labo7.model.EditableDocument;
import labo7.ui.buttons.EditorButton;
import labo7.ui.shortcuts.KeyboardShortcut;
import labo7.ui.shortcuts.ShortcutManager;

@SuppressWarnings("serial")
public class Editor extends JFrame{

	
	private EditableDocument model;
	
	private Command toggleInsert;
	private Command cutCommand;
	private Command pasteCommand;
	private Command copyCommand;
	private Command majCommand;
	private Command minCommand;
	private Command twitCommand;
	private Command undoCommand;
	private Command redoCommand;
	private Command editCommand;
	
	private CommandLog commandLog;
	
	
	private ShortcutManager shortcuts;

	
	private EditorLabel charCount;
	private EditorCheckBox insert;
	private EditorTextArea textBox;
	

	private EditorButton copyButton;
	private EditorButton cutButton;
	private EditorButton pasteButton;
	private EditorButton minButton;
	private EditorButton twitButton;
	private EditorButton majButton;
	
	
	private EditorButton redo;
	private EditorButton undo;

	public Editor(EditableDocument doc) {

		setModel(doc);
		setSize(800,600);
		setTitle("TwitEdit");		

		
		Font font =new Font("Arial",Font.BOLD,20);
		JPanel background = new JPanel();
		background.setLayout(new BoxLayout(background ,BoxLayout.X_AXIS));
		
		//Bo�te de texte
		textBox= new EditorTextArea(500, 400,model, commandLog);
		textBox.setFont(font);	
		textBox.setLineWrap(true);
		textBox.setWrapStyleWord(true);
		
		//Gestionnaire de clavier pour les raccourcis
		shortcuts= new ShortcutManager(); 
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(shortcuts);
		
		
		/*
		 * Initialisation du panneau contenant les boutons
		 */		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel,BoxLayout.Y_AXIS));
		buttonsPanel.setMaximumSize(new Dimension(130,600));
		
		copyButton = new EditorButton("Copier",textBox,model);
		cutButton = new EditorButton("Couper",textBox,model);
		pasteButton = new EditorButton("Coller",textBox,model);
		majButton = new EditorButton("MAJUSCULES",textBox,model);
		minButton = new EditorButton("minuscules",textBox,model);
		twitButton = new EditorButton("Twitterize",textBox,model);		
		
		buttonsPanel.add(Box.createVerticalGlue());
		buttonsPanel.add(copyButton);
		buttonsPanel.add(cutButton);
		buttonsPanel.add(pasteButton);
		buttonsPanel.add(majButton);
		buttonsPanel.add(minButton);
		buttonsPanel.add(twitButton);			
		buttonsPanel.add(Box.createVerticalGlue());
		
		background.add(buttonsPanel);				
		background.add(Box.createHorizontalGlue());
		
		
		/*
		 * Initialisation du panneau contenant la bo�te de texte et les autres contr�les
		 */		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));			
			
	
		//Compteur de caract�res
		charCount=new EditorLabel();
		charCount.setModel(model);
		charCount.setFont(font);
		textPanel.add(charCount);
		textPanel.add(textBox);
	
		//Panneau avec undo, redo et insere
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new BoxLayout(controlPanel,BoxLayout.X_AXIS));		
		
		undo = new EditorButton("undo",textBox,model);
		redo = new EditorButton("redo",textBox,model);
		
		insert = new EditorCheckBox("Insertion");
		insert.setSelected(true);
		controlPanel.add(undo);
		controlPanel.add(redo);
		controlPanel.add(insert);
		
		textPanel.add(controlPanel);		
		background.add(textPanel);		
		
		
		//Ajout du panneau principal dans le frame
		add(background);		
		
		//Pr�parations finales
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public void setModel(EditableDocument doc) {
		model=doc;		
	}

	public void initCommands(CommandLog log) {
		
		commandLog = log;
		
		cutCommand = CommandFactory.getInstance().createCutCommand(model, textBox);
		pasteCommand = CommandFactory.getInstance().createPasteCommand(model, textBox);
		copyCommand = CommandFactory.getInstance().createCopyCommand(model, textBox);
		majCommand = CommandFactory.getInstance().createMajCommand(model, textBox);
		minCommand = CommandFactory.getInstance().createMinCommand(model, textBox);
		twitCommand = CommandFactory.getInstance().createTwitCommand(model, textBox);
		toggleInsert = CommandFactory.getInstance().createToggleInsertCommand(model, insert);
		undoCommand = CommandFactory.getInstance().createUndoCommand();
		redoCommand = CommandFactory.getInstance().createRedoCommand();
		editCommand = CommandFactory.getInstance().createEditTextCommand(model, textBox);
		
		cutButton.storeCommand(cutCommand);
		pasteButton.storeCommand(pasteCommand);
		copyButton.storeCommand(copyCommand);
		majButton.storeCommand(majCommand);
		minButton.storeCommand(minCommand);
		twitButton.storeCommand(twitCommand);
		undo.storeCommand(undoCommand);
		redo.storeCommand(redoCommand);
		textBox.storeCommand(editCommand);
		
		insert.storeCommand(toggleInsert);
		
		KeyboardShortcut copyShortcut = new KeyboardShortcut(KeyEvent.VK_C, true);
		copyShortcut.storeCommand(copyCommand);
		shortcuts.addShortcut(copyShortcut);
		
		KeyboardShortcut pasteShortcut = new KeyboardShortcut(KeyEvent.VK_V, true);
		pasteShortcut.storeCommand(pasteCommand);
		shortcuts.addShortcut(pasteShortcut);
		
		KeyboardShortcut CutShortcut = new KeyboardShortcut(KeyEvent.VK_X, true);
		CutShortcut.storeCommand(cutCommand);
		shortcuts.addShortcut(CutShortcut);
		
		KeyboardShortcut undoShortcut = new KeyboardShortcut(KeyEvent.VK_Z, true);
		undoShortcut.storeCommand(undoCommand);
		shortcuts.addShortcut(undoShortcut);
		
		KeyboardShortcut redoShortcut = new KeyboardShortcut(KeyEvent.VK_Y, true);
		redoShortcut.storeCommand(redoCommand);
		shortcuts.addShortcut(redoShortcut);
	}

}