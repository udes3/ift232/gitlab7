package labo7.ui.menu;

import javax.swing.JPopupMenu;

import commands.CommandFactory;
import commands.CommandLog;
import labo7.model.EditableDocument;
import labo7.ui.EditorTextArea;

public class EditorMenu extends JPopupMenu{

	private static final long serialVersionUID = 1L;
	
	private EditableDocument doc;
	private EditorTextArea textBox;
	
	private EditorMenuItem item;
	
	public EditorMenu(EditableDocument d, EditorTextArea t) {
		
		doc = d;
		textBox = t;
	}
	
	public void initItem() {
		if(textBox.getSelectedText() != null) {
			item = new EditorMenuItem("copy", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createCopyCommand(doc, textBox));
			add(item);
			
			item = new EditorMenuItem("cut", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createCutCommand(doc, textBox));
			add(item);
			
			item = new EditorMenuItem("maj", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createMajCommand(doc, textBox));
			add(item);
			
			item = new EditorMenuItem("min", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createMinCommand(doc, textBox));
			add(item);
		} else {
			item = new EditorMenuItem("paste", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createPasteCommand(doc, textBox));
			add(item);
		}
		
		if(!CommandFactory.getInstance().getLog().emptyUndo()) {
			item = new EditorMenuItem("undo", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createUndoCommand());
			add(item);
		}
		
		if(!CommandFactory.getInstance().getLog().emptyRedo()) {
			item = new EditorMenuItem("redo", doc, textBox);
			item.storeCommand(CommandFactory.getInstance().createRedoCommand());
			add(item);
		}
	}
}
